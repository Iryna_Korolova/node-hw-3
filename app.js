const fs = require('fs');
const express = require('express');
const cors = require('cors');
const mongoose = require('mongoose');
const auth = require('./middlewares/auth');
const authRouter = require('./routes/auth');
const userRouter = require('./routes/user');
const truckRouter = require('./routes/truck');
const loadRouter = require('./routes/load');
const driver = require('./middlewares/driver');
const port = 8080;
require('dotenv').config();
const app = express();

app.use(cors());
app.use(express.json());

app.use((req, res, next) => {
  const log = `${new Date().toLocaleString()} ${req.method} ${req.originalUrl}`;
  fs.appendFile('logs.log', log + '\n', function(error) {
    if (error) {
      return;
    }
  });
  next();
});
app.use('/api/auth', authRouter);
app.use('/api/users', [auth, userRouter]);
app.use('/api/trucks', [auth, driver, truckRouter]);
app.use('/api/loads', [auth, loadRouter]);

mongoose.connect(
    `mongodb+srv://${process.env.DB_LOGIN}:${process.env.DB_PASSWORD}@cluster0.rhkmc.mongodb.net/myFirstDatabase?retryWrites=true&w=majority`,
);

app.listen(port, function() {
  console.log(`Uber server starts on port ${port}`);
});
