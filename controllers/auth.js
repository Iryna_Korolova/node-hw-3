const User = require('../models/user');
const bcrypt = require('bcrypt');
const jwt = require('jsonwebtoken');
require('dotenv').config();

exports.register = async function(req, res, next) {
  const {email, password, role} = req.body;
  if (!email || !password || !role) {
    return res
        .status(400)
        .send({message: 'Email or password or role is missing'});
  }
  try {
    const hash = await bcrypt.hash(password, 1);
    const newUser = new User({email, password: hash, role});
    await newUser.save();
    res.send({
      message: 'Profile created successfully',
    });
  } catch (error) {
    res.status(500).send(error);
  }
};
exports.login = async function(req, res, next) {
  const {email, password} = req.body;
  if (!email || !password) {
    return res.status(400).send({message: 'Email or password not found'});
  }
  const user = await User.findOne({email});
  if (!user) {
    return res.status(400).send({message: 'User not found'});
  }
  const isMatch = await bcrypt.compare(password, user.password);
  if (!isMatch) {
    return res.status(400).send({message: 'Incorrect password'});
  }
  return res.status(200).send({
    jwt_token: jwt.sign(
        {id: user._id, email: user.email, role: user.role},
        process.env.TOKEN_KEY,
    ),
  });
};
exports.forgotPassword = async function(req, res, next) {
  const {email} = req.body;
  if (!email) {
    return res.status(400).send({message: 'Email not found'});
  }
  const user = await User.findOne({email});
  if (!user) {
    return res.status(400).send({message: 'User not found'});
  }
  return res.status(200).send({
    message: 'New password sent to your email address',
  });
};
