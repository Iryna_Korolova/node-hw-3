const User = require('../models/user');
const jwt = require('jsonwebtoken');
require('dotenv').config();

module.exports = function auth(req, res, next) {
  if (!req.headers.authorization) {
    return res.status(400).send({message: 'Unauthorized'});
  }
  jwt.verify(
      req.headers.authorization.split(' ')[1],
      process.env.TOKEN_KEY,
      async (err, payload) => {
        if (err) return res.status(400).send({message: 'Unauthorized'});
        else if (payload) {
          const user = await User.findOne({_id: payload.id});
          req.userId = user._id;
          req.userRole = user.role;
          next();
        }
      },
  );
};
