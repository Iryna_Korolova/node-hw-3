module.exports = function driver(req, res, next) {
  if (req.userRole === 'DRIVER') {
    next();
  } else {
    return res.status(400).send({message: 'Forbidden'});
  }
};
