module.exports = function shipper(req, res, next) {
  if (req.userRole === 'SHIPPER') {
    next();
  } else {
    return res.status(400).send({message: 'Forbidden'});
  }
};

