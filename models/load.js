const mongoose = require('mongoose');

const states = [
  'En route to Pick Up',
  'Arrived to Pick Up',
  'En route to delivery',
  'Arrived to delivery',
];

const LoadSchema = new mongoose.Schema({
  created_by: {
    type: String,
  },
  assigned_to: {
    type: String,
    default: '',
  },
  name: {
    type: String,
  },
  state: {
    type: String,
    set: function(v) {
      if (!v) {
        return states[0];
      }
      const stateIdx = states.findIndex((state) => state === v);
      if (stateIdx > -1) {
        return states[stateIdx + 1] || states[states.length - 1];
      }
    },
    enum: states,
  },
  payload: {
    type: Number,
  },
  status: {
    type: String,
    enum: ['NEW', 'POSTED', 'ASSIGNED', 'SHIPPED'],
    default: 'NEW',
  },
  pickup_address: {
    type: String,
  },
  delivery_address: {
    type: String,
  },
  dimensions: {
    type: Map,
    of: Number,
  },
  logs: [{message: String, time: {type: Date, default: Date.now}}],
  created_date: {
    type: Date,
    required: true,
    default: Date.now,
  },
});

const Load = mongoose.model('Load', LoadSchema);

module.exports = Load;
