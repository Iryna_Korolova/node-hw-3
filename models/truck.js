const mongoose = require('mongoose');

const truckTypes = {
  'SPRINTER': {
    width: 170,
    length: 300,
    height: 250,
    tonnage: 1700,
  },
  'SMALL STRAIGHT': {
    width: 170,
    length: 500,
    height: 250,
    tonnage: 2500,
  },
  'LARGE STRAIGHT': {
    width: 200,
    length: 700,
    height: 350,
    tonnage: 4000,
  },
};

const TruckSchema = new mongoose.Schema({
  created_by: {
    type: String,
    required: true,
  },
  assigned_to: {
    type: String,
    required: true,
  },
  type: {
    type: String,
    enum: ['SPRINTER', 'SMALL STRAIGHT', 'LARGE STRAIGHT'],
    required: true,
  },
  status: {
    type: String,
    enum: ['IS', 'OL'],
    required: true,
    default: 'IS',
  },
  created_date: {
    type: Date,
    required: true,
    default: Date.now,
  },
  chars: {
    type: Map,
    of: Number,
  },
});

TruckSchema.pre(['updateOne', 'save'], function(next) {
  console.log('pre updateOne', this.id);
  if (!this.id) {
    console.log(this._update);
    this.update({chars: truckTypes[this._update.type]});
  } else {
    this.chars = truckTypes[this.type];
  }
  next();
});

const Truck = mongoose.model('Truck', TruckSchema);

module.exports = Truck;
