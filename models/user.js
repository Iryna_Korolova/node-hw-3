const mongoose = require('mongoose');

const UserSchema = new mongoose.Schema({
  password: {
    type: String,
    required: true,
  },
  email: {
    type: String,
    required: true,
  },
  role: {
    type: String,
    enum: ['SHIPPER', 'DRIVER'],
    required: true,
  },
  created_date: {
    type: Date,
    required: true,
    default: Date.now,
  },
});
const User = mongoose.model('User', UserSchema);

module.exports = User;
