const {Router: router} = require('express');
const {
  create,
  getOne,
  getAll,
  update,
  del,
  post,
  getOneActiveDetailed,
  getOneActive,
  changeState,
} = require('../controllers/load');
const driver = require('../middlewares/driver');
const shipper = require('../middlewares/shipper');
const loadRouter = router();

loadRouter.get('/active', [driver, getOneActive]);
loadRouter.get('/:id/shipping_info', [shipper, getOneActiveDetailed]);
loadRouter.get('/:id', [getOne]);
loadRouter.get('/', [getAll]);
loadRouter.post('/:id/post', [shipper, post]);
loadRouter.post('/', [shipper, create]);
loadRouter.patch('/active/state', [driver, changeState]);
loadRouter.put('/:id', [shipper, update]);
loadRouter.delete('/:id', [shipper, del]);

module.exports = loadRouter;
