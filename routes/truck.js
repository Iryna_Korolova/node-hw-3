const {Router: router} = require('express');
const {
  create,
  getAll,
  getOne,
  assign,
  del,
  update,
} = require('../controllers/truck');
const truckRouter = router();

truckRouter.post('/', create);
truckRouter.get('/', getAll);
truckRouter.get('/:id', getOne);
truckRouter.post('/:id/assign', assign);
truckRouter.delete('/:id', del);
truckRouter.put('/:id', update);

module.exports = truckRouter;
